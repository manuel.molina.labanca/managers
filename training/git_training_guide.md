## Background 

* In this section, there will be a couple Q/A type responses that are commonly asked during CLI or GIT training.   
* Understanding GIT will strengthen and speed up your understanding of dbt and GitLab products. 

**Disclaimer**: If the links below no longer work, are inaccurate, or is hard to follow, please feel free to make a commit to remove it from this training guide. As you go through this guide, please feel free to contribute or change anything below.   


### Trees, Trees, and more Trees 
*  In the [command line training guide](https://gitlab.com/gitlab-data/managers/blob/master/training/command_line_training_guide.md), we discussed how the command line responds to prompts for the Apple File System (MacOS's File Directory System), such as `cd` or `mkdir`. 
*  We also discussed how the **file directory systems** are organized in a tree like structure where the root directory `/` is the base of the tree. [Click here to read the file cabinet analogy](https://docstore.mik.ua/orelly/unix3/upt/ch01_14.htm). 
*  GIT also stores and uses the [tree data structures concepts](https://iq.opengenus.org/how-git-uses-trees-data-structure-concepts/): 
 

### Git Basics Topics


### Git Intermediate Topics
1. Merging code from master branch into your branch. 
    1. `git fetch` 
        1. This command fetches all the new changes and prepares it for checkout.  
        1. This command could also be modified to `git fetch origin`
    1. `git checkout -b "branch_name"` 
        1. This command checks out the branch you are currently working on in your MR. 
        1. Alternatively, type `git checkout "type_in_the_issue_number"` <then press tab to find your branch.>
        1. If you want to go back to the master branch, just type `git checkout "origin/master"` but you don't have to for merging code. 
    1. `git merge master` 
        1. This command will stage, add the files that are different, prompt you to add a commit message and then merge it into master. 
    1. In the event of a merge conflict, select between your branch's changes, the target branch's changes, or a new change. 
        1. `git add .`
            1. This command will add any files that had a difference to the stage.
        1. `git commit -m "your_commit_message"`
            1. This command will add a message for the commit
    1. `git push` 
        1. This command will push the code into the master branch. After this, check your MR and see if you can see the new commit. 
        1. Alternatively, type `git push origin "master"`

### Git Advanced Topics


## References
*  GIT Training Guides 
    *  Beginner 
        *  [GitLab Basics - Start using Git on the command line](https://docs.gitlab.com/ce/gitlab-basics/start-using-git.html)
        *  [GitLab basics guides](https://docs.gitlab.com/ce/gitlab-basics/README.html)
        *  [Most Basic Git Commands with Examples](https://rubygarage.org/blog/most-basic-git-commands-with-examples)
    *  Intermediate 
    *  Advanced 
    *  All 
        *  [Learn from GIT!](https://git-scm.com/book/en/v2/Getting-Started-The-Command-Line)
        *  [Learn Git with Bitbucket Cloud](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud)
*  GIT Cheat Sheets / Housekeeping 
    *  [Cleaning Outdated Branches in Local/Remote Repos](https://railsware.com/blog/git-housekeeping-tutorial-clean-up-outdated-branches-in-local-and-remote-repositories/)
*  Tree Data Structures 
    *  [GIT Tree Data Structure Concepts](https://iq.opengenus.org/how-git-uses-trees-data-structure-concepts/)
    *  [Tree Search](http://how2examples.com/artificial-intelligence/tree-search)
    *  [Tree Traversal](https://en.wikipedia.org/wiki/Tree_traversal)
    *  [Search Tree](https://en.wikipedia.org/wiki/Search_tree)

## Exercises
For all the exercises below, you will need to open the Terminal application by searching `Terminal` on your Mac laptop.


## Food for Thought
