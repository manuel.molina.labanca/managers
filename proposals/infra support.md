
## TL;DR

Given the essential function of our Data Warehouse, the Data Engineering team is proposing a stable counterpart on the Infrastructure team as a first step in stablizing the integrations and integration support for GitLab's proprietary services as data sources for our Data warehouse. This stable counterpart would not only serve the working relationship, but would be a resources assigned to work with the Data Engineering team to establish SLOs and a standardized replication and extraction strategy for these data sources. 

## Business Case

In order to properly support GitLab's business operation the Data Team must have reliable access to GitLab's data. Support and awareness of this need becomes increasingly relevant as we approach an initial public offering. Not only are accurate insights essential for the success of our operation, but we will soon be significantly more accountable to external interests and the metrics we are reporting there. 

The [primary function of the data team](https://about.gitlab.com/handbook/business-ops/data-team/#charter) is maintaining a data warehouse that aims to be the single source of truth for our business data. This requires both work within our data warehouse and the maintenance of the extractions and integrations that bring data into the warehouse. 

One of the most widely used and most essential sources of data for our warehouse is data from our proprietary services, which services are under the stewardship of the [infrastructure team](https://about.gitlab.com/handbook/engineering/infrastructure/#mission). Given the importance of these data sources, the relationship between the Infrastructure team and Data Engineering team (which supports the extractions and integrations mentioned) is one that requires formal recognition and support. 

More simply, if the data team is to support the business with data from our proprietary services, then we will also need the support of the groups responsible for that data. 

## Current State

To date we have a positive, but minimal relationship with the infrastructure team. Dave, Anthony, Ben, Jose, Craig, Cameron and many other team members have demonstrated collaboration and kindness when helping us troubleshoot issues and find solutions. Even still, there isn't sufficient communication on changes and problems. 

Right now we are working toward filling many of those holes, but I worry that an ad hoc approach will only lend ad hoc results and we will continue to find gaps in monitoring and in consistent response from a team that is rightfully focused on other goals. 

### Open and Blocked work
1. **An [OKR/Epic on Data Engineering to implement monitoring on Thanos](https://gitlab.com/groups/gitlab-data/-/epics/60) so that we can better address and notify our internal customers of problems with our data.**
   - In addition to being a good solution to the monitoring needs on the data team, we are optimistic that this can be a shared workspace with Infrastructure, allowing for more collaboration
   - At the moment we are waiting on an endpoint to be added to Thanos config - [infra#9281](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/9281)
   - We're trying to improve our monitoring in a couple other ways while we wait for movement here. 
     - We'd like to be added to the notifications for problems with the databases we're reading from. - [infra#9358](https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/9358)
     - We're also going to improve monitoring with the limited tools we have - [data#3938](https://gitlab.com/gitlab-data/analytics/issues/3938)
 1. **[Discussion around a better technical method for replication](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/9434)**
    - Caused a huge delay in the replica that we weren't notified of and are still back filling.  
 1. **Manual uploads every two weeks to support data extraction that was previously automated. The Plan right now to set up an [API for this](gitlab-services/version-gitlab-com#243). We are already getting pressure to do this [more often](https://gitlab.com/gitlab-data/analytics/-/issues/4040).**

In general I would describe the problem as a lack of awareness across teams for which a solution belongs to both Data and Infrastructure

### Business Impact

  - Christopher Lefelhocz, Senior Director of Development is  unable to get use from his KPIs dashboard, which he relies on to manage the productivity of his team.  Christopher and his directors daily review how the teams are performing in relation to MR rate.  They also look at additional information by slicing the data to see anomalies or other factors.  Directors review with their EMs weekly.  But since there are 8 directors and 50+ EMs this requires a daily update cadence.
  - Hila Qu is trying to iterate on data collection SMAU which is a CEO Q1 OKR, but at the moment we are dependent on manual imports because a change that wasn't communicated and a lost connection to version db. 
  -  Many teams KPIs that are reported in Key Reviews are now unavailable which will affect the E-groups ability to operate. This was discussed [in the most recent key meeting for Product](https://docs.google.com/document/d/1Hmg2r-VGYUtYqlHDoPCzeJPI350Y58JSGVHy-P33UBc/edit#heading=h.gxvvxhu3zhlq)

## Ideal state

Ideally the Data team would be intimately aware of the state of the services and databases we're extracting and the Infrastructure team would be aware of our use cases and integrations. Given the size of GitLab as an organization and the many pressures both teams are under this relationship would be structured around the following pillars:

1. **A stable counterpart on the Infrastructure Tean**
   -  This person would be the most aware of the technical solutions and business needs in the Data Organization and would simplify out communication for better prioritization and less noise and interruption 
1. **Documented and supported SLOs for GitLab's proprietary services as data sources.** 
   - This would facilitate clear communication both between teams and among GitLab leadership. It would give us a space to iterate with measured results. I believe it would allow for the Infrastructure team to more explicitly prioritize their many responsibilities. 
1. **A standardized replication and extraction strategy for these data sources.**
   - Short/Mid term solutions being discussed in [infrastructure #9434](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/9434)

## Next Steps

Given the current state I believe that we need to fundamentally and formally improve our relationship with Infrastructure. The most valuable would be the stable counterpart on Infrastructure. From there we have a venue to discuss SLOs as well as integration and replication services running and needed.

The current priority order of these sources is 
1. gitlab.com
1. version
1. customers
1. license
1. everything else
