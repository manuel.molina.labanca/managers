``` mermaid
graph LR

	subgraph staging
		
		subgraph sfdc
			sfdcAccount
			sfdcOpportunity
			sfdcQuote
		end

		subgraph zuora
			zuoraAccount
			zuoraInvoice
		end
	end 

	subgraph common

		zuoraAccount-->dimAccount
		sfdcAccount-->dimAccount
		zuoraInvoice-->fctInvoice
		sfdcOpportunity-->fctQuote
		sfdcQuote-->fctQuote
	
	end

	subgraph marts
	
		subgraph Sales
			dimAccount-->Pipeline
			fctInvoice-->Pipeline
			fctQuote-->Pipeline
		end

		subgraph Finance
			dimAccount-->ARR
			fctInvoice-->ARR
		end
	end


```

## Staging

Function: 

	- Naming conventions 
	- Data Types

Form: 
	- Ephemeral Models 

## Common

Function: 

	- SSOT for Business data by business process
	- Semantic Layer / Data Dictionary

Form: 

	- Dimensional Schema
	- Materialized Tables

## Marts

Function: 
	
	- Serve specific use cases

Form: 

	- Denormalized for use case
	- Views or Tables based on performance needs